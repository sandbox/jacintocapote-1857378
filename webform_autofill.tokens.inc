<?php

//Functionality for tokens

/**
 * Implement hook_token_info().
 */
function webform_autofill_token_info() {

  $type = array(
    'name' => t('Webform autofill'),
    'description' => t('Tokens related to webform autofill.'),
  );

  $webform = array();

  if (!empty($_SESSION['webform_autofill'])) {
    foreach ($_SESSION['webform_autofill'] as $id_component => $data) {
      $webform[$id_component] = array(
        'name' => $id_component,
        'description' => t('Component webform saved in SESSION'),
      );
    }
  }

  return array(
    'types' => array('webform_autofill' => $type),
    'tokens' => array('webform_autofill' => $webform),
  );
}

/**
 * Implement hook_tokens
 */
function webform_autofill_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'webform_autofill') {
    foreach ($tokens as $name => $original) {
      $replacements[$original] = end($_SESSION['webform_autofill'][$name]);

      //Check special cases
      if (!is_string($replacements[$original])) {
        //Check for date
        if (isset($replacements[$original]['month'])) {
          $replacements[$original] = implode('/', $replacements[$original]);
        }
        elseif (isset($replacements[$original]['hour'])) {
          $replacements[$original] = $replacements[$original]['hour'] .':'. $replacements[$original]['minute'] .' '. $replacements[$original]['ampm'];
        }
      }
    }
  }

  return $replacements;
}

